//
//  ViewController.swift
//  FirstApp
//
//  Created by blet on 10/26/18.
//  Copyright © 2018 blet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //Hello
        // Do any additional setup after loading the view, typically from a nib.
        
        let label = UILabel(frame: CGRect(x: 100, y: 100, width: 100, height: 20))
        label.backgroundColor = .yellow
        label.text = "Hello FirstApp"
        label.textAlignment = .center
        view.addSubview(label)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

